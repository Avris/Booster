KEYS_DIR = ./keys

install:
	if [ ! -d "${KEYS_DIR}" ]; then mkdir -p ${KEYS_DIR}; openssl genrsa  -out ${KEYS_DIR}/private.pem 2048; openssl rsa -in ${KEYS_DIR}/private.pem -outform PEM -pubout -out ${KEYS_DIR}/public.pem; fi
	composer install
	yarn
	bin/console doctrine:migration:migrate -n

start:
	symfony run --daemon yarn dev-server
	symfony serve --daemon
	symfony run --daemon node assets/socket.js

stop:
	symfony server:stop

deploy:
	composer install --no-dev --optimize-autoloader
	yarn
	node_modules/.bin/encore production
	bin/console app:email-css
	bin/console cache:clear
	bin/console doctrine:migration:migrate -n
