# Avris Booster

**Quick start of new projects**

Includes:

 * [Symfony 5.2](https://symfony.com/) with [Twig](https://twig.symfony.com/), forms, mailer,
   [Monolog](https://seldaek.github.io/monolog/), translations, [SUML](https://gitlab.com/Avris/SUML-symfony),
   debug, profiler, var-dumper, maker, HTTP client, HTTP cache...
 * [Doctrine ORM](https://www.doctrine-project.org/)
 * [Webpack Encore](https://github.com/symfony/webpack-encore)
 * [Bootstrap 4.4](https://getbootstrap.com/) with [Native JavaScript](https://thednp.github.io/bootstrap.native/), RWD
 * [FontAwesome](https://fontawesome.com/) with [Optimiser](https://gitlab.com/Avris/FontAwesomeOptimiser)
 * [Twitter Emoji](https://twemoji.twitter.com/) with [backend generation](https://gitlab.com/Avris/Twemoji)
 * Fonts: [Baloo Thambi 2](https://fonts.google.com/specimen/Baloo+Thambi+2) & [JetBrains Mono](https://www.jetbrains.com/lp/mono/)
 * Makefile
 * User management: sign up / login / remind password va email code, MFA, [social login](https://socialconnect.lowl.io/), avatars, impersonation
 * Example [Terms of Service](/terms)
 * [Matomo traffic analytics](https://matomo.org/)
 * Error pages
 * Social media meta tags
 * Locale support
 * [Websocket](https://socket.io/)
 * Notifications (browser & push)
 * Relative time

## Installation

As a fork:

    mkdir <project_name>
    cd <project_name>
    git init
    git remote add upstream git@gitlab.com:Avris/Booster.git
    git pull upstream master
    git remote add origin <project_repo>
    git push origin master
    
Or with composer:

    composer create-project avris/booster <project_name>
    cd <project_name>

Configure, install dependencies & start dev server:

    cp .env .env.local
    nano .env.local
    make install
    make start

## Copyright

* **Author:** Andrea Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
