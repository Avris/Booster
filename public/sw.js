const baseUrl = self.registration.scope.replace(/\/+$/,'');

self.addEventListener('push', function(event) {
    const data = JSON.parse(event.data.text());

    self.registration.showNotification(data.text, {data});
});

self.addEventListener('notificationclick', function(event) {
    event.notification.close();
    const link = event.notification.data.link;

    event.waitUntil(clients.matchAll({type: 'window'}).then(function(clientList) {
        for (let client of clientList) {
            if ((client.url === link || client.url === baseUrl + link) && 'focus' in client) {
                return client.focus();
            }
        }
        if (clients.openWindow) {
            return clients.openWindow(baseUrl + link);
        }
    }));
});
