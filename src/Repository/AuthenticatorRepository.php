<?php

namespace App\Repository;

use App\Entity\Authenticator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Authenticator|null find($id, $lockMode = null, $lockVersion = null)
 * @method Authenticator|null findOneBy(array $criteria, array $orderBy = null)
 * @method Authenticator[]    findAll()
 * @method Authenticator[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class AuthenticatorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Authenticator::class);
    }

    public function findValidByTypeAndPayload(string $type, array $payload): ?Authenticator
    {
        $qb = $this->createQueryBuilder('a')
            ->andWhere('a.type = :type')
            ->setParameter('type', $type)
            ->andWhere('a.validUntil is null or a.validUntil >= :now')
            ->setParameter('now', new \DateTimeImmutable());

        foreach ($payload as $key => $value) {
            $qb->andWhere('a.payload LIKE :' . $key)
                ->setParameter($key, '%' . $value . '%');
        }

        /** @var Authenticator $authenticator */
        foreach ($qb->getQuery()->getResult() as $authenticator) {
            foreach ($payload as $key => $value) {
                if ($authenticator->get($key) !== $value) {
                    continue;
                }
            }

            return $authenticator;
        }

        return null;
    }
}
