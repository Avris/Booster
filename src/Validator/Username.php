<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
final class Username extends Constraint
{
    public $message = 'Username can only contain letters, numbers, dashes, underscores and dots.';
}
