<?php

namespace App\Validator;

use App\Entity\User;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

final class UsernameValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Username) {
            throw new UnexpectedTypeException($constraint, Username::class);
        }

        if ($value === null || $value === '') {
            return;
        }

        if (!preg_match('#^' . User::USERNAME_FORMAT . '$#Uui', $value)) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
