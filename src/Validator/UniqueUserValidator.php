<?php

namespace App\Validator;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

final class UniqueUserValidator extends ConstraintValidator
{
    private UserProviderInterface $userProvider;
    private TokenStorageInterface $tokenStorage;

    public function __construct(UserProviderInterface $userProvider, TokenStorageInterface $tokenStorage)
    {
        $this->userProvider = $userProvider;
        $this->tokenStorage = $tokenStorage;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UniqueUser) {
            throw new UnexpectedTypeException($constraint, UniqueUser::class);
        }

        if ($value === null || $value === '') {
            return;
        }

        $currentUser = $this->tokenStorage->getToken()->getUser();
        if ($currentUser instanceof User && ($currentUser->getUsername() === $value || $currentUser->getEmail() === $value)) {
            return;
        }

        try {
            $this->userProvider->loadUserByUsername($value);

            $this->context->buildViolation($constraint->message)->addViolation();
        } catch (UsernameNotFoundException $e) {

        }
    }
}
