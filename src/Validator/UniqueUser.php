<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
final class UniqueUser extends Constraint
{
    public $message = 'Such user already exists';
}
