<?php

namespace App\Controller;

use App\Data\ChangeEmail;
use App\Data\DisposableEmailReceiver;
use App\Data\Login;
use App\Data\LoginCode;
use App\Data\MfaCode;
use App\Data\Registration;
use App\Data\UserToken;
use App\Entity\Authenticator;
use App\Entity\User;
use App\Form\ChangeEmailType;
use App\Form\DeleteAccountType;
use App\Form\LoginCodeType;
use App\Form\LoginType;
use App\Form\MfaCodeType;
use App\Form\RegistrationType;
use App\Repository\AuthenticatorRepository;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use SocialConnect\Auth\Service;
use Sonata\GoogleAuthenticator\GoogleAuthenticatorInterface;
use Sonata\GoogleAuthenticator\GoogleQrUrl;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\RememberMe\RememberMeServicesInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use function Symfony\Component\String\u;

/**
 * @Route("/user")
 */
final class UserController extends AbstractController
{
    private const CODE_SESSION_KEY = 'code';
    private const CODE_INVALID = '[invalid]';
    private const CODE_TTL = 15;

    private const REGISTER_SESSION_KEY = 'register';

    private const CHANGE_EMAIL_SESSION_KEY = 'changeEmail';

    private const MFA_SECRET_SESSION_KEY = 'mfaSecret';
    private const MFA_CHALLENGE_KEY = 'mfaChallenge';

    private EntityManagerInterface $em;
    private TokenStorageInterface $tokenStorage;
    private SessionInterface $session;
    private RememberMeServicesInterface $rememberMeServices;

    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        SessionInterface $session,
        RememberMeServicesInterface $rememberMeServices
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->session = $session;
        $this->rememberMeServices = $rememberMeServices;
    }

    /**
     * @Route("/login")
     */
    public function login(
        Request $request,
        UserProviderInterface $userProvider,
        Mailer $mailer
    ) {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_user_account');
        }

        $form = $this->createForm(LoginType::class, $login = new Login());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $dev = false;
            if ($this->getParameter('kernel.environment') === 'dev' && u($login->identifier)->endsWith('+')) {
                $login->identifier = rtrim($login->identifier, '+');
                $dev = true;
            }

            try {
                $user = $userProvider->loadUserByUsername($login->identifier);
            } catch (UsernameNotFoundException $e) {
                $user = null;
            }
            $code = $this->generateCode();
            $receiver = $user ?: new DisposableEmailReceiver($login->identifier, $request->getLocale());
            if (!$user && !$login->isEmail()) {
                $code = self::CODE_INVALID;
                $receiver = null;
            }

            if ($receiver) {
                if ($dev) {
                    $code = '999999';
                }
                $authenticator = new Authenticator(
                    $user,
                    Authenticator::TYPE_EMAIL,
                    ['email' => $receiver->getEmail(), 'code' => $code],
                    new \DateTimeImmutable('+' . self::CODE_TTL . ' minutes')
                );
                $this->em->persist($authenticator);
                $this->em->flush();

                if (!$dev) {
                    $mailer->send($receiver, 'login', ['code' => $code]);
                }
            }

            $this->session->set(self::CODE_SESSION_KEY, [$code, new \DateTimeImmutable()]);

            return $this->redirectToRoute('app_user_logincode');
        }

        return $this->render('user/login.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function loginForm(Service $socialConnect, ?FormView $form = null)
    {
        return $this->render('user/loginForm.html.twig', [
            'socialProviders' => array_keys($socialConnect->getConfig()['provider']),
            'form' => $form ?? $this->createForm(LoginType::class)->createView(),
        ]);
    }

    /**
     * @Route("/login/code")
     */
    public function loginCode(
        Request $request,
        AuthenticatorRepository $authenticatorRepository,
        TranslatorInterface $translator
    ) {
        list($sessionCode, $generatedAt) = $this->session->get(
            self::CODE_SESSION_KEY,
            [null, new \DateTimeImmutable('-' . (self::CODE_TTL + 1) . ' minutes')]
        );

        if (!$sessionCode || $generatedAt < new \DateTimeImmutable('-' . self::CODE_TTL . ' minutes')) {
            $this->session->remove(self::CODE_SESSION_KEY);

            return $this->redirectToRoute('app_user_login');
        }

        $form = $this->createForm(LoginCodeType::class, $loginCode = new LoginCode());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $submittedCode = $loginCode->code;
            if (!hash_equals(self::CODE_INVALID, $sessionCode)
                && hash_equals($sessionCode, $submittedCode)
                && $authenticator = $authenticatorRepository->findValidByTypeAndPayload(Authenticator::TYPE_EMAIL, ['code' => $submittedCode])
            ) {
                $this->session->remove(self::CODE_SESSION_KEY);
                if ($this->session->get(self::CHANGE_EMAIL_SESSION_KEY) === $authenticator->get('email')) {
                    $this->session->remove(self::CHANGE_EMAIL_SESSION_KEY);

                    $authenticator->getUser()->setEmail($authenticator->get('email'));
                    $this->em->persist($authenticator->getUser());
                    $this->em->flush();

                    return $this->redirectToRoute('app_user_account');
                }

                $this->session->set(self::REGISTER_SESSION_KEY, $authenticator->getId());

                $authenticator->invalidate();
                $this->em->persist($authenticator);
                $this->em->flush();

                return $this->redirectToRoute('app_user_register');
            }

            $form->addError(new FormError($translator->trans('user.login.code.invalid')));
        }

        return $this->render('user/loginCode.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/login/mfa")
     */
    public function mfa(
        Request $request,
        AuthenticatorRepository $authenticatorRepository,
        TranslatorInterface $translator,
        GoogleAuthenticatorInterface $mfa
    ) {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_user_account');
        }

        $auth = $authenticatorRepository->find($this->session->get(self::MFA_CHALLENGE_KEY));
        if (!$auth) {
            return $this->redirectToRoute('app_user_login');
        }

        $form = $this->createForm(MfaCodeType::class, $mfaCode = new MfaCode());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($mfa->checkCode($auth->get('secret'), $mfaCode->code)) {
                $this->session->remove(self::MFA_CHALLENGE_KEY);

                return $this->doLogin($request, new UserToken($auth));
            } else if (hash_equals($auth->get('fallback'), $mfaCode->code)) {
                $auth->invalidate();
                $this->em->persist($auth);
                $this->em->flush();

                $this->session->remove(self::MFA_CHALLENGE_KEY);

                return $this->doLogin($request, new UserToken($auth));
            }

            $form->addError(new FormError($translator->trans('user.login.code.invalid')));
        }

        return $this->render('user/mfa.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/login/{provider}")
     */
    public function loginSocial(
        string $provider,
        Request $request,
        Service $socialConnect,
        TranslatorInterface $translator,
        AuthenticatorRepository $authenticatorRepository,
        UserProviderInterface $userProvider
    ) {
        if (!$request->query->count()) {
            return $this->redirect($socialConnect->getProvider($provider)->makeAuthUrl());
        }

        try {
            $provider = $socialConnect->getProvider($provider);

            if ($request->query->has('error')) {
                throw new \Exception($request->query->get('error'));
            }
            if ($request->query->has('denied')) {
                throw new \Exception($request->query->get('denied'));
            }

            $accessToken = $provider->getAccessTokenByRequestParameters($request->query->all());

            $userData = $provider->getIdentity($accessToken);
            if (!$userData->email) {
                throw new \Exception('Missing email');
            }

            $user = $this->getUser();
            $authenticator = $authenticatorRepository->findValidByTypeAndPayload($provider->getName(), ['id' => (string) $userData->id]);
            if ($user && $authenticator) {
                $authenticator->invalidate();
                $this->em->persist($authenticator);
                $authenticator = null;
            }
        } catch (\Exception $e) {
            $this->addFlash('danger', $translator->trans('user.social.error', ['%provider%' => ucfirst($provider->getName())]));

            return $this->redirectToRoute('app_user_login');
        }

        if ($user) {
            try {
                $duplicate = $userProvider->loadUserByUsername($userData->email);

                if ($duplicate !== $user) {
                    $this->addFlash('danger', $translator->trans('user.social.taken', ['%email%' => $userData->email, '%provider%' => $provider->getName()]));

                    return $this->redirectToRoute('app_user_login');
                }
            } catch (UsernameNotFoundException $e) {}
        }

        if (!$authenticator) {
            $authenticator = new Authenticator(
                $user,
                $provider->getName(),
                [
                    'id' => (string) $userData->id,
                    'username' => (string) $userData->username,
                    'name' => (string) $userData->fullname,
                    'email' => $userData->email,
                    'avatar' => (string) $userData->pictureURL,
                    'accessToken' => $accessToken->getToken(),
                ]
            );
        }

        $this->em->persist($authenticator);
        $this->em->flush();

        $this->session->set(self::REGISTER_SESSION_KEY, $authenticator->getId());

        return $this->redirectToRoute('app_user_register');
    }

    /**
     * @Route("/register")
     */
    public function register(
        Request $request,
        AuthenticatorRepository $authenticatorRepository
    ) {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_user_account');
        }

        $authenticator = $authenticatorRepository->find($this->session->get(self::REGISTER_SESSION_KEY));
        if (!$authenticator) {
            return $this->redirectToRoute('app_home_index');
        }

        if ($authenticator->getUser()) {
            $this->session->remove(self::REGISTER_SESSION_KEY);

            $mfa = $authenticator->getUser()->getAuthenticator(Authenticator::TYPE_MFA);
            if ($mfa) {
                $this->session->set(self::MFA_CHALLENGE_KEY, $mfa->getId());

                return $this->redirectToRoute('app_user_mfa');
            }

            return $this->doLogin($request, new UserToken($authenticator));
        }

        $form = $this->createForm(RegistrationType::class, $registration = Registration::fromAuthenticator($authenticator));

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = new User($authenticator->get('email'), $registration->username, $request->getLocale());
            $authenticator->setUser($user);

            $this->em->persist($user);
            $this->em->persist($authenticator);
            $this->em->flush();

            $this->session->remove(self::REGISTER_SESSION_KEY);

            return $this->doLogin($request, new UserToken($authenticator));
        }

        return $this->render('user/registration.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/account")
     * @IsGranted("ROLE_USER")
     */
    public function account(
        Request $request,
        Service $socialConnect,
        Mailer $mailer,
        GoogleAuthenticatorInterface $mfa,
        TranslatorInterface $translator
    ) {
        /** @var User $user */
        $user = $this->getUser();

        $emailForm = $this->createForm(ChangeEmailType::class, $changeEmail = new ChangeEmail());
        $emailForm->handleRequest($request);
        if ($emailForm->isSubmitted() && $emailForm->isValid()) {
            $code = $this->generateCode();

            $authenticator = new Authenticator(
                $user,
                Authenticator::TYPE_EMAIL,
                ['email' => $changeEmail->email, 'code' => $code],
                new \DateTimeImmutable('+' . self::CODE_TTL . ' minutes')
            );
            $this->em->persist($authenticator);
            $this->em->flush();

            $mailer->send(new DisposableEmailReceiver($changeEmail->email, $user->getLocale()), 'login', ['code' => $code]);

            $this->session->set(self::CODE_SESSION_KEY, [$code, new \DateTimeImmutable()]);
            $this->session->set(self::CHANGE_EMAIL_SESSION_KEY, $changeEmail->email);

            return $this->redirectToRoute('app_user_logincode');
        }

        if (!$this->session->has(self::MFA_SECRET_SESSION_KEY)) {
            $this->session->set(self::MFA_SECRET_SESSION_KEY, $mfa->generateSecret());
        }

        $mfaForm = $this->createForm(MfaCodeType::class, $mfaCode = new MfaCode(), ['autofocus' => false]);
        $mfaForm->handleRequest($request);
        if ($mfaForm->isSubmitted() && $mfaForm->isValid()) {
            if ($mfa->checkCode($this->session->get(self::MFA_SECRET_SESSION_KEY), $mfaCode->code)) {
                $fallback = $this->generateCode();

                $this->em->persist(new Authenticator($this->getUser(), Authenticator::TYPE_MFA, [
                    'secret' => $this->session->get(self::MFA_SECRET_SESSION_KEY),
                    'fallback' => $fallback,
                ]));
                $this->em->flush();

                $this->session->remove(self::MFA_SECRET_SESSION_KEY);

                $this->addFlash('success', $translator->trans('user.mfa.initSuccess', ['%fallback%' => $fallback]));

                return $this->redirectToRoute('app_user_account');
            } else {
                $mfaForm->addError(new FormError($translator->trans('user.mfa.invalid')));
            }
        }

        $deleteForm = $this->createForm(DeleteAccountType::class);
        $deleteForm->handleRequest($request);
        if ($deleteForm->isSubmitted() && $deleteForm->isValid()) {
            $this->em->remove($user);
            $this->em->flush();

            return $this->redirectToRoute('app_user_logout');
        }

        return $this->render('user/account.html.twig', [
            'socialProviders' => array_keys($socialConnect->getConfig()['provider']),
            'emailForm' => $emailForm->createView(),
            'deleteForm' => $deleteForm->createView(),
            'mfaForm' => $mfaForm->createView(),
            'mfaSecret' => $this->session->get(self::MFA_SECRET_SESSION_KEY),
            'mfaQr' => GoogleQrUrl::generate($translator->trans('main.title'), $this->session->get(self::MFA_SECRET_SESSION_KEY))
        ]);
    }

    /**
     * @Route("/account/avatar/{source}")
     * @IsGranted("ROLE_USER")
     */
    public function selectAvatar(string $source, Service $socialConnect)
    {
        if ($source === 'gravatar' || !array_key_exists($source, $socialConnect->getConfig()['provider'])) {
            $source = '';
        }

        /** @var User $user */
        $user = $this->getUser();
        $user->setAvatarSource($source);
        $this->em->persist($user);
        $this->em->flush();

        return $this->redirectToRoute('app_user_account');
    }

    /**
     * @Route("/account/disconnect/{provider}")
     * @IsGranted("ROLE_USER")
     */
    public function disconnect(string $provider)
    {
        /** @var Authenticator $auth */
        $auth = $this->getUser()->getAuthenticator($provider);
        if ($auth) {
            $auth->invalidate();
            $this->em->persist($auth);
            $this->em->flush();
        }

        return $this->redirectToRoute('app_user_account');
    }

    /**
     * @Route("/logout")
     * @IsGranted("ROLE_USER")
     */
    public function logout(Request $request)
    {
        $this->tokenStorage->setToken(null);
        $this->rememberMeServices->loginFail($request);
        $this->session->clear();

        return $this->redirectToRoute('app_home_index');
    }

    private function doLogin(Request $request, UserToken $token)
    {
        $this->tokenStorage->setToken($token);

        $response = $this->redirectToRoute('app_user_account');

        $this->rememberMeServices->loginSuccess($request, $response, $token);

        return $response;
    }

    private function generateCode(): string
    {
        return str_pad((string) random_int(0, 1000000), 6, '0', STR_PAD_LEFT);
    }
}
