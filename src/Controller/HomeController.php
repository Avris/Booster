<?php

namespace App\Controller;

use App\Entity\User;
use App\EventSubscriber\LocaleSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

final class HomeController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(\Parsedown $parsedown)
    {
        return $this->render('home/index.html.twig', [
            'readme' => $parsedown->parse(file_get_contents($this->getParameter('kernel.project_dir') . '/README.md')),
        ])->setSharedMaxAge(24 * 60 * 60);
    }

    /**
     * @Route("/terms")
     */
    public function terms()
    {
        return $this->render('home/terms.html.twig');
    }

    /**
     * @Route("/locale/{locale}")
     */
    public function locale(string $locale, Request $request, SessionInterface $session, EntityManagerInterface $em)
    {
        $locales = $this->getParameter('locales');
        if (array_key_exists($locale, $locales)) {
            $session->set(LocaleSubscriber::LOCALE_SESSION_KEY, $locale);
            $user = $this->getUser();
            if ($user instanceof User) {
                $user->setLocale($locale);
                $em->persist($user);
                $em->flush();
            }
        }

        return $this->redirect($request->server->get('HTTP_REFERER') ?? $this->generateUrl('app_home_index'));
    }
}
