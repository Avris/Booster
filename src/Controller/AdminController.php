<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use SocialConnect\Auth\Service;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @IsGranted("ROLE_ADMIN")
 */
final class AdminController extends AbstractController
{
    /**
     * @Route("/users")
     */
    public function users(UserRepository $userRepository, Service $socialConnect)
    {
        return $this->render('admin/users.html.twig', [
            'users' => $userRepository->findAll(),
            'socialProviders' => array_keys($socialConnect->getConfig()['provider']),
        ]);
    }
}
