<?php

namespace App\Controller;

use App\Entity\Authenticator;
use App\Entity\User;
use App\Service\NotificationService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/notifications")
 * @IsGranted("ROLE_USER")
 */
final class NotificationController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index()
    {
        return $this->render('notification/index.html.twig');
    }

    /**
     * @Route("/read")
     */
    public function read(NotificationService $notificationService)
    {
        $notificationService->markRead($this->getUser());

        return $this->json('ok');
    }

    /**
     * @Route("/subscribe", methods={"POST"})
     */
    public function subscribe(Request $request, EntityManagerInterface $em)
    {
        $payload = json_decode($request->getContent(), true);

        /** @var Authenticator $auth */
        foreach ($this->getUser()->getAuthenticators('push') as $auth) {
            if ($auth->getPayload()['keys']['auth'] === $payload['keys']['auth']) {
                return $this->json('ok (existing)');
            }
        }

        $auth = new Authenticator($this->getUser(), 'push', $payload);
        $em->persist($auth);
        $em->flush();

        return $this->json('ok (new)');
    }

    /**
     * @Route("/test/{username}/{message}", condition="'%kernel.environment%' === 'dev'")
     * //IsGranted("ROLE_ADMIN")
     */
    public function test(User $user, string $message, NotificationService $notificationService)
    {
        $notification = $notificationService->notify(
            $user,
            'test',
            ['from' => $this->getUser()->getId(), 'message' => $message],
            $this->generateUrl('app_home_terms'),
        );

        dump($notification);die;
    }
}
