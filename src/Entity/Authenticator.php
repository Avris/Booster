<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AuthenticatorRepository")
 */
class Authenticator
{
    public const TYPE_EMAIL = 'email';
    public const TYPE_MFA = 'mfa';

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=26)
     */
    private string $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="authenticators")
     */
    private ?User $user;

    /**
     * @ORM\Column(type="string", length=36)
     */
    private string $type;

    /**
     * @ORM\Column(type="json")
     */
    private array $payload = [];

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeInterface $validUntil;

    public function __construct(?User $user, string $type, array $payload, \DateTimeInterface $validUntil = null)
    {
        $this->id = new Ulid();
        $this->user = $user;
        $this->type = $type;
        $this->payload = $payload;
        $this->createdAt = new \DateTimeImmutable();
        $this->validUntil = $validUntil;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        if (!$this->user) {
            $this->user = $user;
        }

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getPayload(): array
    {
        return $this->payload;
    }

    public function get(string $key, $default = null)
    {
        return $this->payload[$key] ?? $default;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function isValid(): bool
    {
        return $this->validUntil === null || $this->validUntil >= new \DateTimeImmutable();
    }

    public function invalidate(): self
    {
        if ($this->isValid()) {
            $this->validUntil = new \DateTimeImmutable();
        }

        return $this;
    }
}
