<?php

namespace App\Entity;

use App\Data\EmailReceiver;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Ulid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, EmailReceiver
{
    public const USERNAME_FORMAT = '[A-Za-z0-9_.-]+';

    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_ADMIN = 'ROLE_ADMIN';

    public const DEFAULT_ROLES = [ self::ROLE_USER ];

    public const AVATAR_FALLBACK = '';

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=26)
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $email;

    /**
     * @ORM\Column(type="string", length=36)
     */
    private string $username;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private string $locale;

    /**
     * @ORM\Column(type="json")
     */
    private array $roles = [];

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $avatarSource;

    /**
     * @var ArrayCollection|Authenticator[]
     * @ORM\OneToMany(targetEntity="App\Entity\Authenticator", mappedBy="user", cascade={"remove"})
     */
    private $authenticators;

    /**
     * @var ArrayCollection|Notification[]
     * @ORM\OneToMany(targetEntity="App\Entity\Notification", mappedBy="user", orphanRemoval=true)
     * @ORM\OrderBy({"timestamp": "DESC"})
     */
    private $notifications;

    public function __construct(string $email, string $username, string $locale, array $roles = self::DEFAULT_ROLES)
    {
        $this->id = new Ulid();
        $this->email = $email;
        $this->username = $username;
        $this->locale = $locale;
        $this->roles = $roles;
        $this->createdAt = new \DateTimeImmutable();
        $this->avatarSource = self::AVATAR_FALLBACK;
        $this->authenticators = new ArrayCollection();
        $this->notifications = new ArrayCollection();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function isAdmin(): bool
    {
        return in_array(self::ROLE_ADMIN, $this->roles);
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getAvatarSource(): ?string
    {
        return $this->avatarSource;
    }

    public function setAvatarSource(string $avatarSource): self
    {
        $this->avatarSource = $avatarSource;

        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return Authenticator[]|Collection
     */
    public function getAuthenticators(string $type): Collection
    {
        return $this->authenticators->filter(function (Authenticator $auth) use ($type) {
            return $auth->getType() === $type && $auth->isValid();
        });
    }

    public function getAuthenticator(string $type): ?Authenticator
    {
        return $this->getAuthenticators($type)->first() ?: null;
    }

    public function getPassword()
    {
        return null;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function countUnreadNotifications(): int
    {
        return $this->notifications->filter(function (Notification $notification) {
            return !$notification->isRead();
        })->count();
    }
}
