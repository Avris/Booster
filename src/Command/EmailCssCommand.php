<?php

namespace App\Command;

use Symfony\Component\Asset\Packages;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use function Symfony\Component\String\u;

class EmailCssCommand extends Command
{
    protected static $defaultName = 'app:email-css';

    private Packages $packages;
    private string $projectDir;

    public function __construct(Packages $packages, string $projectDir, string $name = null)
    {
        parent::__construct($name);
        $this->packages = $packages;
        $this->projectDir = $projectDir;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $source = $this->packages->getUrl('build/app.css');
        if (u($source)->startsWith('/')) {
            $source = $this->projectDir . '/public/build' . $source;
        }
        $target = $this->projectDir . '/assets/email/email.css';

        $output->writeln(sprintf('Copying <info>%s</info> to <info>%s</info>', $source, $target));
        file_put_contents($target, file_get_contents($source));

        return 0;
    }
}
