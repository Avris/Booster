<?php

namespace App\Form;

use App\Data\ChangeEmail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ChangeEmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', InlineSubmitType::class, [
                'label' => 'user.email',
                'btnIcon' => 'edit',
                'btnText' => 'user.account.change',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ChangeEmail::class,
        ]);
    }
}
