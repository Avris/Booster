<?php

namespace App\Form;

use App\Data\Login;
use App\Data\Registration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'user.username',
                'attr' => [
                    'autofocus' => true,
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => 'user.email',
                'disabled' => true,
                'attr' => [
                    'readonly' => true,
                ],
            ])
            ->add('terms', CheckboxType::class, [
                'label' => 'user.registration.terms',
                'label_attr' => [
                    'class' => 'checkbox-custom',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'user.registration.submit',
                'attr' => [
                    'class' => 'btn btn-primary btn-block',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Registration::class,
        ]);
    }
}
