<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class InlineSubmitType extends AbstractType
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['btnClass'] = $options['btnClass'];
        $view->vars['btnIcon'] = $options['btnIcon'];
        $view->vars['btnText'] = $options['btnText'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'btnClass' => 'btn btn-outline-primary',
            'btnIcon' => 'paper-plane',
            'btnText' => '',
        ]);
    }

    public function getParent()
    {
        return TextType::class;
    }
}
