<?php

namespace App\Form;

use App\Data\MfaCode;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class MfaCodeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', InlineSubmitType::class, [
                'label' => false,
                'btnIcon' => 'sign-in',
                'attr' => [
                    'autofocus' => $options['autofocus'],
                    'placeholder' => 'user.mfa.placeholder',
                    'inputmode' => 'numeric',
                    'pattern' => "[0-9]{6}",
                    'autocomplete' => 'one-time-code',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MfaCode::class,
            'autofocus' => true,
        ]);

        $resolver->setAllowedTypes('autofocus', 'bool');
    }
}
