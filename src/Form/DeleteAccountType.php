<?php

namespace App\Form;

use App\Data\DeleteAccount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class DeleteAccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('confirmation', CheckboxType::class, [
                'label' => 'user.delete.confirm',
                'label_attr' => [
                    'class' => 'checkbox-custom',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'user.delete.submit',
                'attr' => [
                    'class' => 'btn btn-outline-danger btn-block',
                ],
            ])
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DeleteAccount::class,
        ]);
    }
}
