<?php

namespace App\Form;

use App\Data\Login;
use MeteoConcept\HCaptchaBundle\Form\HCaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('captcha', HCaptchaType::class, [
                'label' => false,
            ])
            ->add('identifier', InlineSubmitType::class, [
                'label' => false,
                'btnIcon' => 'sign-in',
                'attr' => [
                    'autofocus' => true,
                    'placeholder' => 'user.login.identifier',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Login::class,
        ]);
    }
}
