<?php

namespace App\Service;

use App\Entity\Notification;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

final class NotificationService extends AbstractExtension
{
    private EntityManagerInterface $em;
    private SocketClient $socketClient;
    private Environment $twig;

    public function __construct(EntityManagerInterface $em, SocketClient $socketClient, Environment $twig)
    {
        $this->em = $em;
        $this->socketClient = $socketClient;
        $this->twig = $twig;
    }

    public function notify(User $user, string $type, array $params, string $link): Notification
    {
        $notification = new Notification($user, $type, $params, $link);
        $this->em->persist($notification);
        $this->em->flush();

        $data = [
            'type' => $type,
            'html' => $this->twig->render('notification/single.html.twig', ['notification' => $notification]),
            'text' => $this->twig->render('notification/single.txt.twig', ['notification' => $notification]),
            'link' => $notification->getLink(),
        ];

        $this->socketClient->send($user, 'notification', $data);
        foreach ($this->socketClient->push($user, $data) as $auth) {
            $this->em->persist($auth);
        }
        $this->em->flush();

        return $notification;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('parametrise', function (array $params): array {
                $new = [];
                foreach ($params as $k => $v) {
                    $new['%' . $k . '%'] = $v;
                }
                return $new;
            }),
            new TwigFilter('entities', function (string $str, bool $plainText = false): string {
                return preg_replace_callback('#<(\w+):(\w+)>#Uui', function ($matches) use ($plainText) {
                    list(, $type, $id) = $matches;
                    $object = $this->em->getRepository('App\\Entity\\' . $type)->find($id);

                    return $this->twig->render(sprintf('entity/%s.%s.twig', lcfirst($type), $plainText ? 'txt' : 'html'), ['object' => $object]);
                }, $str);
            }),
        ];
    }

    public function markRead(User $user)
    {
        /** @var Notification $notification */
        foreach ($this->em->getRepository(Notification::class)->findBy(['user' => $user, 'isRead' => false]) as $notification) {
            $notification->markRead();
            $this->em->persist($notification);
        }

        $this->em->flush();
    }
}
