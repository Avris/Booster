<?php

namespace App\Service;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class Terminator implements EventSubscriberInterface
{
    private array $callbacks = [];

    public function attach(callable $callback): self
    {
        $this->callbacks[] = $callback;

        return $this;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::RESPONSE => ['onResponse', -9999],
            KernelEvents::TERMINATE => ['onTerminate'],
        ];
    }

    public function onResponse(ResponseEvent $event)
    {
        if (!count($this->callbacks)) {
            return;
        }

        ignore_user_abort(true);
        set_time_limit(0);

        $headers = $event->getResponse()->headers;
        $headers->set('Connection', 'close');
        $headers->set('Content-Length', (string) strlen($event->getResponse()->getContent()));
        $headers->set('Content-Encoding', 'none');
    }

    public function onTerminate(TerminateEvent $event)
    {
        foreach ($this->callbacks as $callback) {
            $callback($event);
        }
    }
}
