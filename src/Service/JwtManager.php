<?php

namespace App\Service;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\Token;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class JwtManager extends AbstractExtension
{
    private string $baseUrl;
    private Key $privateKey;
    private Builder $builder;
    private Sha256 $signer;

    public function __construct(string $baseUrl, string $privateKey)
    {
        $this->baseUrl = $baseUrl;
        $this->privateKey = new Key($privateKey);
        $this->builder = new Builder();
        $this->signer = new Sha256();
    }

    public function issueToken(string $uid): Token
    {
        $now = new \DateTimeImmutable();
        return $this->builder->issuedBy($this->baseUrl)
            ->permittedFor($this->baseUrl)
            ->issuedAt($now->format('U'))
            ->expiresAt($now->modify('+1 day')->format('U'))
            ->withClaim('uid', $uid)
            ->getToken($this->signer, $this->privateKey);
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('jwt', [$this, 'issueToken']),
        ];
    }
}
