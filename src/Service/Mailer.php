<?php

namespace App\Service;

use App\Data\EmailReceiver;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final class Mailer
{
    private MailerInterface $mailer;
    private string $from;
    private Terminator $terminator;
    private string $fallbackLocale;
    private string $subjectTemplate;
    private TranslatorInterface $translator;
    private string $logo;

    public function __construct(
        MailerInterface $mailer,
        string $from,
        Terminator $terminator,
        string $fallbackLocale,
        string $subjectTemplate,
        TranslatorInterface $translator,
        string $logo
    ) {
        $this->mailer = $mailer;
        $this->from = $from;
        $this->terminator = $terminator;
        $this->fallbackLocale = $fallbackLocale;
        $this->subjectTemplate = $subjectTemplate;
        $this->translator = $translator;
        $this->logo = $logo;
    }

    public function send(EmailReceiver $receiver, string $type, array $vars = [])
    {
        $this->terminator->attach(function () use ($receiver, $type, $vars) {
            $locale = $receiver->getLocale() ?? $this->fallbackLocale;

            $email = (new TemplatedEmail())
                ->from($this->from)
                ->to($receiver->getEmail())
                ->embedFromPath($this->logo, 'logo')
                ->subject(sprintf(
                    $this->subjectTemplate,
                    $this->translator->trans('main.title', [], null, $locale),
                    $this->translator->trans('email.' . $type . '.subject', $this->varsAsParams($vars), null, $locale)
                ))
                ->htmlTemplate('emails/' . $type . '.html.twig')
                ->textTemplate('emails/' . $type . '.txt.twig')
                ->context($vars);

            $this->mailer->send($email);
        });
    }

    private function varsAsParams(array $vars): array
    {
        $newVars = [];
        foreach ($vars as $key => $value) {
            $newVars['%' . $key . '%'] = $value;
        }

        return $newVars;
    }
}
