<?php

namespace App\Service;

use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

final class RelativeTime extends AbstractExtension
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getRelative(\DateTimeInterface $dt, ?\DateTimeInterface $reference = null): string
    {
        $diff = ($reference ?? new \DateTimeImmutable())->diff($dt);

        [$value, $unit] = $this->getMostSignificantUnit($diff);

        if ($unit === 'now') {
            return $this->translator->trans('relativeTime.now');
        }

        $str = $this->translator->trans(
            sprintf(
                'relativeTime.%s.%s',
                $unit,
                $value === 1 ? 'singular' : 'plural'
            ),
            ['%count%' => $value]
        );

        return $this->translator->trans(
            sprintf(
                'relativeTime.%s',
                $diff->invert ? 'ago' : 'in'
            ),
            ['%count%' => $str]
        );
    }

    private function getMostSignificantUnit(\DateInterval $diff): array
    {
        if ($diff->y) {
            return [$diff->y, 'years'];
        } elseif ($diff->m) {
            return [$diff->m, 'months'];
        } elseif ($diff->d) {
            return [$diff->d, 'days'];
        } elseif ($diff->h) {
            return [$diff->h, 'hours'];
        } elseif ($diff->i) {
            return [$diff->i, 'minutes'];
        }

        return [0, 'now'];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('relativeTime', [$this, 'getRelative']),
        ];
    }
}
