<?php

namespace App\Service;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class FlagService extends AbstractExtension
{
    const COUNTRY_MARKERS = [
        'A' => '🇦', 'B' => '🇧', 'C' => '🇨', 'D' => '🇩', 'E' => '🇪', 'F' => '🇫', 'G' => '🇬', 'H' => '🇭',
        'I' => '🇮', 'J' => '🇯', 'K' => '🇰', 'L' => '🇱', 'M' => '🇲', 'N' => '🇳', 'O' => '🇴', 'P' => '🇵',
        'Q' => '🇶', 'R' => '🇷', 'S' => '🇸', 'T' => '🇹', 'U' => '🇺', 'V' => '🇻', 'W' => '🇼', 'X' => '🇽',
        'Y' => '🇾', 'Z' => '🇿',
    ];

    public function getFlag(string $code)
    {
        if (!preg_match('#^[A-Z][A-Z]$#', $code)) {
            throw new \Exception(sprintf('Invalid country code "%s"', $code));
        }

        return self::COUNTRY_MARKERS[$code[0]] . self::COUNTRY_MARKERS[$code[1]];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('flag', [$this, 'getFlag']),
        ];
    }
}
