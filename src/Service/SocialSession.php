<?php

namespace App\Service;

use SocialConnect\Provider\Session\SessionInterface as SocialSessionInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

final class SocialSession implements SocialSessionInterface
{
    private SessionInterface $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function get($key)
    {
        return $this->session->get($key);
    }

    public function set($key, $value)
    {
        $this->session->set($key, $value);
    }

    public function delete($key)
    {
        $this->session->remove($key);
    }
}