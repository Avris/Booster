<?php

namespace App\Service;

use App\Entity\User;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class AvatarService extends AbstractExtension
{
    private const DEFAULT_SIZE = 64;
    private const URL_TEMPLATES = [
        'facebook' => 'https://graph.facebook.com/{id}/picture?type=square&height={size}&width={size}',
    ];

    public function getAvatar(?User $user, ?string $source = null, int $size = self::DEFAULT_SIZE): ?string
    {
        if (!$user) {
            return null;
        }

        if ($source === null) {
            $source = $user->getAvatarSource();
        }

        if ($source) {
            $auth = $user->getAuthenticator($source);
            if (!$auth) {
                return null;
            }

            return $auth->get('avatar')
                ?? $this->printTemplate($source, $auth->get('id'), $size);
        }

        return $this->getGravatar(
            $user->getEmail(),
            $this->getAvi($user->getUsername(), $size),
            $size,
        );
    }

    private function printTemplate(string $source, string $id, int $size): ?string
    {
        if (!array_key_exists($source, self::URL_TEMPLATES)) {
            return null;
        }

        return strtr(self::URL_TEMPLATES[$source], [
            '{id}' => $id,
            '{size}' => $size,
        ]);
    }

    private function getGravatar(string $email, string $fallback, int $size): string
    {
        return sprintf(
            'https://www.gravatar.com/avatar/%s?d=%s&s=%s',
            md5(strtolower(trim($email))),
            urlencode($fallback),
            $size,
        );
    }

    private function getAvi(string $username, int $size): string
    {
        return sprintf(
            'https://avi.avris.it/%s/%s.png',
            $size,
            rtrim(strtr(base64_encode($username), '+/', '-_'), '='),
        );
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('avatar', [$this, 'getAvatar']),
        ];
    }
}
