<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class SocketClient
{
    private HttpClientInterface $client;
    private JwtManager $jwtManager;
    private string $socketUrl;

    public function __construct(HttpClientInterface $client, JwtManager $jwtManager, string $socketUrl)
    {
        $this->client = $client;
        $this->jwtManager = $jwtManager;
        $this->socketUrl = $socketUrl;
    }

    public function send(?User $to, string $type, $message)
    {
        $this->client->request('POST', $this->socketUrl, [
            'json' => [
                'token' => (string) $this->jwtManager->issueToken('__system__'),
                'to' => $to ? $to->getId() : null,
                'type' => $type,
                'message' => $message,
            ],
            'verify_peer' => false,
        ]);
    }

    public function push(?User $to, $message)
    {
        if (!$to) {
            return;
        }

        foreach ($to->getAuthenticators('push') as $auth) {
            $res = $this->client->request('POST', $this->socketUrl . '/push', [
                'json' => [
                    'token' => (string) $this->jwtManager->issueToken('__system__'),
                    'subscription' => $auth->getPayload(),
                    'message' => $message,
                ],
                'verify_peer' => false,
            ]);
            if ($res->getStatusCode() === 410) {
                $auth->invalidate();
                yield $auth;
            }
        }
    }
}
