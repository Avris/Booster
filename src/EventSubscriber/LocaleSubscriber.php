<?php

namespace App\EventSubscriber;

use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final class LocaleSubscriber implements EventSubscriberInterface
{
    public const LOCALE_SESSION_KEY = 'locale';

    private array $locales;
    private string $defaultLocale;
    private TokenStorageInterface $tokenStorage;
    private SessionInterface $session;
    private TranslatorInterface $translator;

    public function __construct(
        array $locales,
        string $defaultLocale,
        TokenStorageInterface $tokenStorage,
        SessionInterface $session,
        TranslatorInterface $translator
    ) {
        $this->locales = $locales;
        $this->defaultLocale = $defaultLocale;
        $this->tokenStorage = $tokenStorage;
        $this->session = $session;
        $this->translator = $translator;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $locale = $this->determineLocale($event->getRequest());
        $event->getRequest()->setLocale($locale);
        $this->translator->setLocale($locale);
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.request' => 'onKernelRequest',
        ];
    }

    private function determineLocale(Request $request): string
    {
        $token = $this->tokenStorage->getToken();
        $user = $token && $token->getUser() instanceof User ? $token->getUser() : null;
        if ($user) {
            return $user->getLocale();
        }

        if ($this->session->has(self::LOCALE_SESSION_KEY)) {
            return $this->session->get(self::LOCALE_SESSION_KEY);
        }

        return $request->getPreferredLanguage(array_keys($this->locales)) ?? $this->defaultLocale;
    }
}
