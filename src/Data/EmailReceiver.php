<?php

namespace App\Data;

interface EmailReceiver
{
    public function getEmail(): ?string;
    public function getLocale(): ?string;
}
