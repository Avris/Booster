<?php

namespace App\Data;

use App\Entity\Authenticator;
use App\Validator\UniqueUser;
use App\Validator\Username;
use Symfony\Component\Validator\Constraints as Assert;

final class Registration
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=5)
     * @Assert\Length(max=16)
     * @UniqueUser()
     * @Username()
     */
    public ?string $username = null;

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @UniqueUser()
     */
    public ?string $email = null;

    /**
     * @Assert\NotBlank()
     */
    public bool $terms = false;

    public static function fromAuthenticator(Authenticator $authenticator)
    {
        $registration = new static();
        $registration->email = $authenticator->get('email');
        $registration->username = $authenticator->get('username', '');

        return $registration;
    }
}
