<?php

namespace App\Data;

use App\Entity\Authenticator;
use App\Validator\UniqueUser;
use App\Validator\Username;
use Symfony\Component\Validator\Constraints as Assert;

final class DeleteAccount
{
    /**
     * @Assert\NotBlank()
     */
    public bool $confirmation = false;
}
