<?php

namespace App\Data;

use Symfony\Component\Validator\Constraints as Assert;

final class Login
{
    private const EMAIL_PATTERN = '/^[a-zA-Z0-9.!#$%&\'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/';

    /**
     * @Assert\NotBlank()
     */
    public ?string $identifier = null;

    public function isEmail(): bool
    {
        return $this->identifier && preg_match(self::EMAIL_PATTERN, $this->identifier);
    }
}
