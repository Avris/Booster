<?php

namespace App\Data;

use Symfony\Component\Validator\Constraints as Assert;

final class LoginCode
{
    /**
     * @Assert\NotBlank()
     * @Assert\Regex("/^[0-9]{6}$/")
     */
    public ?string $code = null;
}
