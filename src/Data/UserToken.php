<?php

namespace App\Data;

use App\Entity\Authenticator;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Validator\Constraints as Assert;

final class UserToken extends UsernamePasswordToken
{
    public function __construct(Authenticator $authenticator)
    {
        parent::__construct($authenticator->getUser(), $authenticator, 'database_users', $authenticator->getUser()->getRoles());
    }
}
