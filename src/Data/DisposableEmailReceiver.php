<?php

namespace App\Data;

final class DisposableEmailReceiver implements EmailReceiver
{
    private ?string $email;
    private ?string $locale;

    public function __construct(?string $email, ?string $locale = null)
    {
        $this->email = $email;
        $this->locale = $locale;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }
}
