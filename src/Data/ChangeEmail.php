<?php

namespace App\Data;

use App\Entity\User;
use App\Validator\UniqueUser;
use Symfony\Component\Validator\Constraints as Assert;

final class ChangeEmail
{
    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @UniqueUser()
     */
    public ?string $email = null;
}
