const app = require('express')();
const fs = require('fs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const webpush = require('web-push');

dotenv.config({path: __dirname + '/../.env.local'});
dotenv.config({path: __dirname + '/../.env'});

const port = parseInt(process.env.SOCKET_URL.match(/:(\d+)$/)[1]);
const url = process.env.BASE_URL;
const cert = fs.readFileSync(__dirname + '/../keys/public.pem');
webpush.setVapidDetails(url, process.env.VAPID_PUBLIC, process.env.VAPID_PRIVATE);

class ConnectionList {
    constructor() {
        this.users = {};
    }

    register(user, socket) {
        if (this.users[user] === undefined) {
            this.users[user] = {};
        }

        this.users[user][socket.id] = socket;
    }

    unregister(user, socket) {
        if (this.users[user] === undefined) {
            return;
        }

        delete this.users[user][socket.id];
    }

    get(user) {
        if (user === null) {
            return Object.values(this.users).reduce((carry, val) => [...carry, ...Object.values(val)], []);
        }

        if (this.users[user] === undefined) {
            return [];
        }

        return Object.values(this.users[user]);
    }
}

const http = require(process.env.DOMAIN_CERT ? 'https' : 'http');

const connections = new ConnectionList();

const validateJwt = (token) => {
    if (!token) {
        return null;
    }

    try {
        return jwt.verify(token, cert, { audience: url, issuer: url }).uid;
    } catch (e) {
        return null;
    }
};

app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send('socket.io');
});

app.post('/', (req, res) => {
    try {
        const {token, to, type, message} = req.body;

        const user = validateJwt(token);
        if (user !== '__system__') {
            res.statusCode = 401;
            return res.send('Unauthorized');
        }

        console.log('Received message to', to || 'everyone', type, message);

        for (let s of connections.get(to)) {
            console.log('Sending message to', s.id);
            s.emit(type, message);
        }

        res.send('ok');
    } catch (e) {
        console.error(e);
        res.statusCode = 400;
        return res.send('Invalid request');
    }
});

app.post('/push', async (req, res) => {
    try {
        const {token, subscription, message} = req.body;

        const user = validateJwt(token);
        if (user !== '__system__') {
            res.statusCode = 401;
            return res.send('Unauthorized');
        }

        try {
            await webpush.sendNotification(subscription, JSON.stringify(message));
        } catch (e) {
            console.error(e);
            res.statusCode = 410;
            return res.send('Unsubscribed');
        }

        res.send('ok');
    } catch (e) {
        console.error(e);
        res.statusCode = 400;
        return res.send('Invalid request');
    }
});

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', url);
    res.header('Access-Control-Allow-Headers', 'authorization,content-type,x-requested-with');
    res.header('Access-Control-Allow-Credentials', 'true');
    next();
});

const options = process.env.DOMAIN_CERT ? {
    key: fs.readFileSync(process.env.DOMAIN_PRIV_KEY).toString(),
    cert: fs.readFileSync(process.env.DOMAIN_CERT).toString()
}: {};

const server = http.createServer(options, app).listen(port, function() {
    console.log('listening on *:' + port);
});

const io = require('socket.io')(server);

io.on('connection', (socket) => {
    const user = validateJwt(socket.request._query.token);

    console.log('Connected user', user, socket.id);
    connections.register(user, socket);

    socket.on('disconnect', function() {
        console.log('Disconnected user', user, socket.id);
        connections.unregister(user, socket);
    });
});
