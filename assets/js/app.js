import '../css/app.scss';

import '../images/favicon.png';
import '../images/banner.png';
import '../images/logoBlack.svg';
import '../images/logoWhite.svg';

import 'bootstrap.native/dist/bootstrap-native-v4';

import './notifications';
