import socket from './socket';
import {_el, _on} from "avris-vanillin";
import soundFile from '../sound/ding.mp3';
import icon from '../images/favicon.png';
import base64toUint8Array from './base64toUint8Array';

const $count = document.querySelectorAll('.notification-count');
const sound = new Audio(soundFile);

const user = document.body.dataset.user;
const applicationServerKey = base64toUint8Array(document.body.dataset.vapidPublic)

const subscribeToPush = async () => {
    const registration = await navigator.serviceWorker.register('/sw.js');
    let subscription = await registration.pushManager.getSubscription();
    if (user) {
        const options = { applicationServerKey, userVisibleOnly: true };
        subscription = await registration.pushManager.subscribe(options);
        const response = await fetch(document.body.dataset.pushSubscribe, {
            method: 'post',
            headers: { 'Content-Type': 'application/json', },
            body: JSON.stringify(subscription)
        });
    } else if (!user && subscription) {
        await subscription.unsubscribe();
    }
}

const observer = new IntersectionObserver(entries => {
    entries.forEach((entry) => {
        if (!entry.isIntersecting) {
            return;
        }

        fetch(document.body.dataset.notificationsRead).then(_ => {
            setTimeout(_ => {
                entry.target.querySelectorAll('.notification').forEach($n => {
                    $n.classList.remove('bg-info');
                    $count.forEach($c => {
                        $c.innerHTML = '0';
                        $c.parentElement.classList.remove('active');
                    });
                });
            }, 2000);
        });
    })
});

const handlePermissionStatus = function() {
    if (!('Notification' in window)) {
        return;
    }
    document.querySelectorAll('[data-notification-status]').forEach($st => {
        if (Notification.permission === $st.dataset.notificationStatus) {
            $st.classList.remove('d-none');
        } else {
            $st.classList.add('d-none');
        }
    });
    if (Notification.permission === 'granted') {
        subscribeToPush();
    }
};
_on('[data-notification-status="default"]', 'click', function () {
    Notification.requestPermission().then(p => {
        handlePermissionStatus();
    });
});
handlePermissionStatus();

document.querySelectorAll('.notification-list').forEach($el => {
    observer.observe($el);
    socket.on('notification', function (data) {
        $el.insertBefore(_el(data.html), $el.firstChild);

        $count.forEach($c => {
            $c.innerHTML = parseInt($c.innerHTML.trim()) + 1 + '';
            $c.parentElement.classList.add('active');
        });

        sound.play();

        if ('Notification' in window && !('PushManager' in window) && Notification.permission === 'granted') {
            const notification = new Notification(data.text, {icon});
            notification.onclick = function(event) {
                event.preventDefault();
                window.open(data.link);
            }
        }
    });
});
